//给你一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 a，b，c ，使得 a + b + c = 0 ？请你找出所有和为 0 且不重
//复的三元组。 
//
// 注意：答案中不可以包含重复的三元组。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [-1,0,1,2,-1,-4]
//输出：[[-1,-1,2],[-1,0,1]]
// 
//
// 示例 2： 
//
// 
//输入：nums = []
//输出：[]
// 
//
// 示例 3： 
//
// 
//输入：nums = [0]
//输出：[]
// 
//
// 
//
// 提示： 
//
// 
// 0 <= nums.length <= 3000 
// -105 <= nums[i] <= 105 
// 
// Related Topics 数组 双指针 
// 👍 3317 👎 0

package com.zys.leetcode.editor.cn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSum {
    public static void main(String[] args) {

        Solution solution = new ThreeSum().new Solution();
        System.out.println(solution.threeSum(new int[]{-1,1,-1,0,0,0,2,0,4,-4}));
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        if (nums.length < 3){
            return result;
        }
        Arrays.sort(nums);
        int n = nums.length;
        for (int i = 0; i < n; ++i){
            if (nums[0] > 0 || nums[n - 1] < 0){
                break;
            }
            if (i > 0 && nums[i] == nums[i - 1]){
                continue;
            }
            int second = i + 1;
            int third = n - 1;
            int taget = -nums[i];
            while (second < third) {
                if (nums[second] + nums[third] == taget) {
                    result.add(new ArrayList<>(Arrays.asList(nums[i], nums[second], nums[third])));
                    second++;
                    third--;
                    while (second < third && nums[second] == nums[second - 1]) {
                        ++second;
                    }
                    while (second < third && nums[third] == nums[third + 1]) {
                        --third;
                    }
                } else if (nums[second] + nums[third] > taget) {
                    --third;
                } else {
                   ++second;
                }
            }
        }
        return result;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}