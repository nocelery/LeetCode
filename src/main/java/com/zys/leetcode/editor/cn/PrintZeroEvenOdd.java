//假设有这么一个类： 
//
// class ZeroEvenOdd {
//  public ZeroEvenOdd(int n) { ... }      // 构造函数
//  public void zero(printNumber) { ... }  // 仅打印出 0
//  public void even(printNumber) { ... }  // 仅打印出 偶数
//  public void odd(printNumber) { ... }   // 仅打印出 奇数
//}
// 
//
// 相同的一个 ZeroEvenOdd 类实例将会传递给三个不同的线程： 
//
// 
// 线程 A 将调用 zero()，它只输出 0 。 
// 线程 B 将调用 even()，它只输出偶数。 
// 线程 C 将调用 odd()，它只输出奇数。 
// 
//
// 每个线程都有一个 printNumber 方法来输出一个整数。请修改给出的代码以输出整数序列 010203040506... ，其中序列的长度必须为 2n
//。 
//
// 
//
// 示例 1： 
//
// 输入：n = 2
//输出："0102"
//说明：三条线程异步执行，其中一个调用 zero()，另一个线程调用 even()，最后一个线程调用odd()。正确的输出为 "0102"。
// 
//
// 示例 2： 
//
// 输入：n = 5
//输出："0102030405"
// 
// 👍 97 👎 0

package com.zys.leetcode.editor.cn;

import java.util.concurrent.Semaphore;
import java.util.function.IntConsumer;

public class PrintZeroEvenOdd {
    public static void main(String[] args) {
//        Solution solution = new PrintZeroEvenOdd().new Solution();
        ZeroEvenOdd zeroEvenOdd = new PrintZeroEvenOdd().new ZeroEvenOdd(5);
        new PrintZeroEvenOdd().new Zero(zeroEvenOdd).start();
        new PrintZeroEvenOdd().new Even(zeroEvenOdd).start();
        new PrintZeroEvenOdd().new Odd(zeroEvenOdd).start();
    }
    class Zero extends Thread{
        private ZeroEvenOdd zeroEvenOdd;
        public Zero(ZeroEvenOdd zeroEvenOdd){
            this.zeroEvenOdd = zeroEvenOdd;
        }
        @Override
        public void run(){
            try {
                zeroEvenOdd.zero(new IntConsumer() {
                    @Override
                    public void accept(int value) {
                        System.out.print(value);
                    }
                });
            } catch (InterruptedException e) {


            }
        }
    }
    class Even extends Thread{
        private ZeroEvenOdd zeroEvenOdd;
        public Even(ZeroEvenOdd zeroEvenOdd){
            this.zeroEvenOdd = zeroEvenOdd;
        }
        @Override
        public void run(){
            try {
                zeroEvenOdd.even(new IntConsumer() {
                    @Override
                    public void accept(int value) {
                        System.out.print(value);
                    }
                });
            } catch (InterruptedException e) {

            }
        }
    }
    class Odd extends Thread{
        private ZeroEvenOdd zeroEvenOdd;
        public Odd(ZeroEvenOdd zeroEvenOdd){
            this.zeroEvenOdd = zeroEvenOdd;
        }
        @Override
        public void run(){
            try {
                zeroEvenOdd.odd(new IntConsumer() {
                    @Override
                    public void accept(int value) {
                        System.out.print(value);
                    }
                });
            } catch (InterruptedException e) {


            }
        }
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class ZeroEvenOdd {
    private int n;
    private int i = 1;
    Semaphore zero = new Semaphore(1);
    Semaphore even = new Semaphore(0);
    Semaphore odd = new Semaphore(0);
    public ZeroEvenOdd(int n) {
        this.n = n;
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public void zero(IntConsumer printNumber) throws InterruptedException {
        while (i < n){
            zero.acquire();
            printNumber.accept(0);
            if (i % 2 == 0){
                even.release();
            }
            else {
                odd.release();
            }
        }
    }

    public void even(IntConsumer printNumber) throws InterruptedException {
        while (i < n){
            even.acquire();
            printNumber.accept(i);
            ++i;
            zero.release();
        }
    }

    public void odd(IntConsumer printNumber) throws InterruptedException {
        while (i < n){
            odd.acquire();
            printNumber.accept(i);
            ++i;
            zero.release();
        }
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}