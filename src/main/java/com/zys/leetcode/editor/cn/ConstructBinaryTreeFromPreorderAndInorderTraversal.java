//根据一棵树的前序遍历与中序遍历构造二叉树。 
//
// 注意: 
//你可以假设树中没有重复的元素。 
//
// 例如，给出 
//
// 前序遍历 preorder = [3,9,20,15,7]
//中序遍历 inorder = [9,3,15,20,7] 
//
// 返回如下的二叉树： 
//
//     3
//   / \
//  9  20
//    /  \
//   15   7 
// Related Topics 树 深度优先搜索 数组 
// 👍 1059 👎 0

package com.zys.leetcode.editor.cn;
public class ConstructBinaryTreeFromPreorderAndInorderTraversal {
    public static void main(String[] args) {
        Solution solution = new ConstructBinaryTreeFromPreorderAndInorderTraversal().new Solution();
        solution.buildTree(new int[]{3,9,20,15,7},new int[]{9,3,15,20,7});
    }
    //leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node. */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class Solution {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return build(preorder, inorder,0,0,inorder.length - 1);
    }
    public TreeNode build(int[] preorder, int[] inorder, int pi, int il, int ih){
//      序列遍历完毕，或者节点没有孩子节点
        if (pi >= preorder.length || il > ih){
            return null;
        }
        TreeNode root = new TreeNode(preorder[pi]);
        int i = il;
//      在中序遍历中找到当前节点root
        for (i = il; (preorder[pi] != inorder[i] && i < ih); ++i){}
//      如果找到当前节点，递归
        if (inorder[i] == preorder[pi]) {
//          pi加一是因为当前节点已经创建了，前序的下一个节点便是左孩子
//          左子树的节点在il到i - 1中
            root.left = build(preorder, inorder, pi + 1, il, i - 1);
        }
        if (i < ih) {
//          前序数组跳过i - il + 1个左孩子节点，右子树节点在i + 1到ih中
            root.right = build(preorder, inorder, pi + i - il + 1, i + 1, ih);
        }
        return root;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}