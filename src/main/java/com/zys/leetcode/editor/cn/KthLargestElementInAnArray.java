//给定整数数组 nums 和整数 k，请返回数组中第 k 个最大的元素。 
//
// 请注意，你需要找的是数组排序后的第 k 个最大的元素，而不是第 k 个不同的元素。 
//
// 
//
// 示例 1: 
//
// 
//输入: [3,2,1,5,6,4] 和 k = 2
//输出: 5
// 
//
// 示例 2: 
//
// 
//输入: [3,2,3,1,2,4,5,5,6] 和 k = 4
//输出: 4 
//
// 
//
// 提示： 
//
// 
// 1 <= k <= nums.length <= 10⁴ 
// -10⁴ <= nums[i] <= 10⁴ 
// 
// Related Topics 数组 分治 快速选择 排序 堆（优先队列） 👍 1260 👎 0

package com.zys.leetcode.editor.cn;

import java.math.BigDecimal;

/**
* @author zys
* @date 2021-09-08 11:11:28
*/
public class KthLargestElementInAnArray {
    public static void main(String[] args) {
        Solution solution = new KthLargestElementInAnArray().new Solution();
        BigDecimal bigDecimal = new BigDecimal(124);
        System.out.println(bigDecimal);
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int findKthLargest(int[] nums, int k) {
        return partition(nums,0,nums.length - 1,k);
    }

    public int partition(int[] nums, int low, int high, int k){
        int pivot = quickSort(nums,low,high);
        if (pivot + 1 == k) {
            return nums[pivot];
        }
        if (pivot + 1 > k) {
            return partition(nums,low, pivot - 1, k);
        }else {
            return partition(nums,pivot + 1,high, k);
        }
    }

    public int quickSort(int[] nums, int low, int high) {
        int pivot = nums[low];
        while (low < high) {
            while (low<high && nums[high] <= pivot){
                high--;
            }
            nums[low] = nums[high];
            while (low<high && nums[low] >= pivot){
                low++;
            }
            nums[high] = nums[low];
        }
        nums[low] = pivot;
        return low;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}