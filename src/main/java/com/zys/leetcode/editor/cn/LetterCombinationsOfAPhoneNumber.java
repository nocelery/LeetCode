//给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。 
//
// 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。 
//
// 
//
// 
//
// 示例 1： 
//
// 
//输入：digits = "23"
//输出：["ad","ae","af","bd","be","bf","cd","ce","cf"]
// 
//
// 示例 2： 
//
// 
//输入：digits = ""
//输出：[]
// 
//
// 示例 3： 
//
// 
//输入：digits = "2"
//输出：["a","b","c"]
// 
//
// 
//
// 提示： 
//
// 
// 0 <= digits.length <= 4 
// digits[i] 是范围 ['2', '9'] 的一个数字。 
// 
// Related Topics 深度优先搜索 递归 字符串 回溯算法 
// 👍 1301 👎 0

package com.zys.leetcode.editor.cn;

import java.util.ArrayList;
import java.util.List;

public class LetterCombinationsOfAPhoneNumber {
    public static void main(String[] args) {
        Solution solution = new LetterCombinationsOfAPhoneNumber().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public char[][] letters = new char[][]{
            {},{}, {'a','b','c'},{'d','e','f'},{'g','h','i'},
            {'j','k','l'},{'m','n','o'},{'p','q','r','s'},{'t','u','v'},
            {'w','x','y','z'}
    };
    List<String> result = new ArrayList<>();
    public List<String> letterCombinations(String digits) {
        if (digits.length() == 0){
            return result;
        }
        int[] nums = new int[digits.length()];
        for (int i = 0; i < nums.length; ++i){
            nums[i] = Integer.parseInt(digits.substring(i,i + 1));
        }
        combination(nums,0,"");
        return result;
    }
    public void combination(int[] digits,int index,String s){
        int point = digits[index];
        if (index == digits.length - 1){
            for (int i = 0; i < letters[point].length; ++i){
                String temp = s + String.valueOf(letters[point][i]);
                result.add(temp);
            }
            return;
        }
        for (int i = 0; i < letters[point].length; ++i){
            combination(digits,index + 1,s + String.valueOf(letters[point][i]));
        }

    }
}
//leetcode submit region end(Prohibit modification and deletion)

}