//在一个由 '0' 和 '1' 组成的二维矩阵内，找到只包含 '1' 的最大正方形，并返回其面积。 
//
// 
//
// 示例 1： 
//
// 
//输入：matrix = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"]
//,["1","0","0","1","0"]]
//输出：4
// 
//
// 示例 2： 
//
// 
//输入：matrix = [["0","1"],["1","0"]]
//输出：1
// 
//
// 示例 3： 
//
// 
//输入：matrix = [["0"]]
//输出：0
// 
//
// 
//
// 提示： 
//
// 
// m == matrix.length 
// n == matrix[i].length 
// 1 <= m, n <= 300 
// matrix[i][j] 为 '0' 或 '1' 
// 
// Related Topics 数组 动态规划 矩阵 👍 910 👎 0

package com.zys.leetcode.editor.cn;

import com.zys.leetcode.editor.cn.util.ArrayUtil;

/**
* @author zys
* @date 2021-11-08 15:32:47
*/
public class MaximalSquare {
    public static void main(String[] args) {
        Solution solution = new MaximalSquare().new Solution();
        String array = "[\"1\",\"1\",\"1\",\"1\",\"0\"],[\"1\",\"1\",\"1\",\"1\",\"0\"],[\"1\",\"1\",\"1\",\"1\",\"1\"],[\"1\",\"1\",\"1\",\"1\",\"1\"],[\"0\",\"0\",\"1\",\"1\",\"1\"]";
        char[][] chars = ArrayUtil.stringToTwoDimenCharArray(array);
        int i = solution.maximalSquare(chars);
        System.out.println(i);
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int maximalSquare(char[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;
        //保存每个点所能得到的最大正方形的边长
        int[][] dp = new int[m + 1][n + 1];
        //最后的面积
        int maxSquare = 0;
        for (int i = 0; i < m + 1; i++) {
            dp[i][0] = 0;
        }
        for (int i = 0; i < n + 1; i++) {
            dp[0][i] = 0;
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                //无法构成正方形
                if (matrix[i][j] == '0') {
                    dp[i + 1][j + 1] = 0;
                }else {
                    //往左、往上和对角是否0，是则无法和之前的点构成正方形
                    if ((i > 0 && matrix[i - 1][j] == '0') ||
                            (j > 0 && matrix[i][j - 1] == '0') ||
                            ((i >0 && j > 0) && matrix[i - 1][j - 1] == '0')) {
                        dp[i + 1][j + 1] = 1;
                    }else {
                        //取左、上和对角中最小的dp值，以保证dp[i+1][j+1]保存正方形边长是正确的
                        dp[i + 1][j + 1] = Math.min(dp[i][j],Math.min(dp[i][j + 1],dp[i + 1][j])) + 1;
                    }
                }
                maxSquare = Math.max(maxSquare,dp[i + 1][j + 1] * dp[i + 1][j + 1]);
            }
        }
        return maxSquare;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}