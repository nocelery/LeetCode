//给定一个按照升序排列的整数数组 nums，和一个目标值 target。找出给定目标值在数组中的开始位置和结束位置。 
//
// 如果数组中不存在目标值 target，返回 [-1, -1]。 
//
// 进阶： 
//
// 
// 你可以设计并实现时间复杂度为 O(log n) 的算法解决此问题吗？ 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [5,7,7,8,8,10], target = 8
//输出：[3,4] 
//
// 示例 2： 
//
// 
//输入：nums = [5,7,7,8,8,10], target = 6
//输出：[-1,-1] 
//
// 示例 3： 
//
// 
//输入：nums = [], target = 0
//输出：[-1,-1] 
//
// 
//
// 提示： 
//
// 
// 0 <= nums.length <= 105 
// -109 <= nums[i] <= 109 
// nums 是一个非递减数组 
// -109 <= target <= 109 
// 
// Related Topics 数组 二分查找 
// 👍 1003 👎 0

package com.zys.leetcode.editor.cn;
public class FindFirstAndLastPositionOfElementInSortedArray {
    public static void main(String[] args) {
        Solution solution = new FindFirstAndLastPositionOfElementInSortedArray().new Solution();
        int[] result = solution.searchRange(new int[]{5, 7, 7, 8, 8, 10}, 8);
        System.out.println(result[0] + " " + result[1]);
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[] searchRange(int[] nums, int target) {
        int begin = -1;//记录第一个位置的下标
        int end = -1;//记录最后位置的下标
        int left = 0;
        int right = nums.length - 1;
        int mid = 0;
//      二分查找，找到taget值的其中一个下标
        while (left <= right){
            mid = (left + right) / 2;
            if (nums[mid] == target){
                break;
            }
            else if (nums[mid] > target){
                right = mid - 1;
            }
            else {
                left = mid + 1;
            }
        }
//      没有taget，返回[-1,-1]
        if (left > right){
            return new int[]{-1,-1};
        }
//      以mid为分界，向左区间寻找第一位置，右区间寻找最后位置
        int beginBorder = mid;
        int endBorder = mid;
//      往左区间寻找，循环结束后，left == beginBorder，为第一位置
        while (left <= beginBorder){
            mid = (left + beginBorder) / 2;
//          如果nums[mid]大于等于taget，则记录位置，区间向左移动
            if (nums[mid] >= target){
                begin = mid;
                beginBorder = mid - 1;
            }
//          区间向右移动
            else {
                left = mid + 1;
            }
        }
//      与上面的查找类似
        while (endBorder <= right){
            mid = (endBorder + right) / 2;
            if (nums[mid] <= target){
                end = mid;
                endBorder = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }
        return new int[]{begin,end};
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}