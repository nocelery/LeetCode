//给定一个二叉树, 找到该树中两个指定节点的最近公共祖先。 
//
// 百度百科中最近公共祖先的定义为：“对于有根树 T 的两个节点 p、q，最近公共祖先表示为一个节点 x，满足 x 是 p、q 的祖先且 x 的深度尽可能大（
//一个节点也可以是它自己的祖先）。” 
//
// 
//
// 示例 1： 
//
// 
//输入：root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
//输出：3
//解释：节点 5 和节点 1 的最近公共祖先是节点 3 。
// 
//
// 示例 2： 
//
// 
//输入：root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4
//输出：5
//解释：节点 5 和节点 4 的最近公共祖先是节点 5 。因为根据定义最近公共祖先节点可以为节点本身。
// 
//
// 示例 3： 
//
// 
//输入：root = [1,2], p = 1, q = 2
//输出：1
// 
//
// 
//
// 提示： 
//
// 
// 树中节点数目在范围 [2, 10⁵] 内。 
// -10⁹ <= Node.val <= 10⁹ 
// 所有 Node.val 互不相同 。 
// p != q 
// p 和 q 均存在于给定的二叉树中。 
// 
// Related Topics 树 深度优先搜索 二叉树 👍 1374 👎 0

package com.zys.leetcode.editor.cn;

import com.zys.leetcode.editor.cn.entity.TreeNode;
import com.zys.leetcode.editor.cn.util.TreeNodeUtil;

import java.util.Stack;

/**
* @author zys
* @date 2021-11-09 15:18:41
*/
public class LowestCommonAncestorOfABinaryTree {
    public static void main(String[] args) {
        Solution solution = new LowestCommonAncestorOfABinaryTree().new Solution();
        Integer[] array = new Integer[]{1,2};
        TreeNode root = TreeNodeUtil.arrayToTreeNode(array);
        TreeNode treeNode = solution.lowestCommonAncestor(root, root, root.left);
        System.out.println(treeNode.val);
    }
    //leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        Stack<TreeNode> stackp = new Stack<>();
        Stack<TreeNode> stackq = new Stack<>();
        find(stackp,root,p);
        find(stackq,root,q);
        return findAncestor(stackp,stackq);
    }

    private TreeNode findAncestor(Stack<TreeNode> stackp, Stack<TreeNode> stackq) {
        Stack<TreeNode> tmp;
        int plen;
        int qlen;
        if (stackp.size() > stackq.size()) {
            tmp = stackp;
            stackp = stackq;
            stackq = tmp;
        }
        plen = stackp.size();
        qlen = stackq.size();
        while (qlen - plen > 0){
            stackq.pop();
            qlen--;
        }
        while (stackp.size() > 1 && stackq.size() > 1) {
            if (stackp.peek() == stackq.peek()) {
                return stackp.pop();
            }
            stackp.pop();
            stackq.pop();
        }
        return stackp.pop();
    }

    public boolean find(Stack<TreeNode> stack, TreeNode root, TreeNode p) {
        stack.push(root);
        if (root == p) {
            return true;
        }
        if (root.left != null) {
            if (find(stack,root.left,p)) {
                return true;
            }
            stack.pop();
        }
        if (root.right != null) {
            if (find(stack,root.right,p)) {
                return true;
            }
            stack.pop();
        }
        return false;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}