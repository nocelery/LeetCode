//给定一个 m x n 二维字符网格 board 和一个字符串单词 word 。如果 word 存在于网格中，返回 true ；否则，返回 false 。 
//
// 单词必须按照字母顺序，通过相邻的单元格内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母不允许被重复使用。 
//
// 
//
// 示例 1： 
//
// 
//输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "AB
//CCED"
//输出：true
// 
//
// 示例 2： 
//
// 
//输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SE
//E"
//输出：true
// 
//
// 示例 3： 
//
// 
//输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "AB
//CB"
//输出：false
// 
//
// 
//
// 提示： 
//
// 
// m == board.length 
// n = board[i].length 
// 1 <= m, n <= 6 
// 1 <= word.length <= 15 
// board 和 word 仅由大小写英文字母组成 
// 
//
// 
//
// 进阶：你可以使用搜索剪枝的技术来优化解决方案，使其在 board 更大的情况下可以更快解决问题？ 
// Related Topics 数组 回溯算法 
// 👍 908 👎 0

package com.zys.leetcode.editor.cn;
public class WordSearch {
    public static void main(String[] args) {
        Solution solution = new WordSearch().new Solution();
//      direction: -1表示起点，0表示向右，1表示向左，2表示向上，3表示向下
//        System.out.println(solution.exist(new char[][]{{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}},
//                "SEE"));
        solution.exist(new char[][]{{'a'}},"a");
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public boolean exist(char[][] board, String word) {
        int m = board.length;
        int n = board[0].length;
//      判断字符数组是否为空，或者字符数少于单词数
        if (m < 1 || n < 1 || m * n < word.length()){
            return false;
        }
        char[] letters = word.toCharArray();
        for (int i = 0; i < m; ++i){
            for (int j = 0; j < n; ++j){
//              找到第一个匹配的字符，开始搜索
                if (board[i][j] == letters[0]){
                    if(search(board,letters,i,j,0,-1)){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public boolean search(char[][] board,char[] letters,int br,int bc,int li,int direction){
//      搜索到单词的最后一个，直接判断是否相同
        if (li == letters.length - 1){
            return board[br][bc] == letters[li];
        }
//      不匹配或者字符被遍历过，则返回false
        if (board[br][bc] != letters[li] || board[br][bc] == '!'){
            return false;
        }
//      标记当前位置，表示已经遍历过
        char temp = board[br][bc];
        board[br][bc] = '!';
        int m = board.length;
        int n = board[0].length;
//      判断是否能够往下搜索：指针必须没有超过数组范围，且不能搜索上次遍历的位置
//      下同
        if (br + 1 < m && direction != 2
                && search(board,letters,br + 1, bc,li + 1,3)) {
            return true;
        }
        if (bc + 1 < n && direction != 1
                && search(board,letters,br, bc + 1,li + 1,0)) {
            return true;
        }
        if (br - 1 >= 0 && direction != 3
                && search(board,letters,br - 1, bc,li + 1,2)) {
            return true;
        }
        if (bc - 1 >= 0 && direction != 0
                && search(board,letters,br, bc - 1,li + 1,1)) {
            return true;
        }
//      恢复标记
        board[br][bc] = temp;
        return false;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}