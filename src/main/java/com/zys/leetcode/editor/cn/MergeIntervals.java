//以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [starti, endi] 。请你合并所有重叠的区间，并返
//回一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间。 
//
// 
//
// 示例 1： 
//
// 
//输入：intervals = [[1,3],[2,6],[8,10],[15,18]]
//输出：[[1,6],[8,10],[15,18]]
//解释：区间 [1,3] 和 [2,6] 重叠, 将它们合并为 [1,6].
// 
//
// 示例 2： 
//
// 
//输入：intervals = [[1,4],[4,5]]
//输出：[[1,5]]
//解释：区间 [1,4] 和 [4,5] 可被视为重叠区间。 
//
// 
//
// 提示： 
//
// 
// 1 <= intervals.length <= 104 
// intervals[i].length == 2 
// 0 <= starti <= endi <= 104 
// 
// Related Topics 排序 数组 
// 👍 932 👎 0

package com.zys.leetcode.editor.cn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.IntFunction;

public class MergeIntervals {
    public static void main(String[] args) {
        Solution solution = new MergeIntervals().new Solution();
        System.out.println(solution.merge(new int[][]{{1, 3}, {2, 6}, {8, 10}, {15, 18}}));
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[][] merge(int[][] intervals) {
//      排序，区间起点小的在前
        Arrays.sort(intervals, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return o1[0] >= o2[0] ?
                        o1[0] > o2[0] ? 1:0
                        :-1;
            }
        });
        int i = 0;
        int n = intervals.length;
        List<int[]> answer = new ArrayList<>();
        while (i < n){
            int[] temp = new int[2];
//          保存无重叠区间的起点
            temp[0] = intervals[i][0];
//          暂存区间的最远终点
            int rightmost = intervals[i][1];
            int j = i + 1;
//          遍历i之后的区间，比较是否有区间的起点大于rightm
            while (j < n && intervals[j][0] <= rightmost){
//              比较并保存最远终点
                rightmost = Math.max(rightmost,intervals[j][1]);
                ++j;
            }
//          保存无重叠区间的终点
            temp[1] = rightmost;
            i = j;
            answer.add(temp);
        }
        return answer.toArray(new int[answer.size()][]);
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}