//数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。 
//
// 
//
// 示例 1： 
//
// 
//输入：n = 3
//输出：["((()))","(()())","(())()","()(())","()()()"]
// 
//
// 示例 2： 
//
// 
//输入：n = 1
//输出：["()"]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= n <= 8 
// 
// Related Topics 字符串 回溯算法 
// 👍 1775 👎 0

package com.zys.leetcode.editor.cn;

import java.util.ArrayList;
import java.util.List;

public class GenerateParentheses {
    public static void main(String[] args) {
        Solution solution = new GenerateParentheses().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    List<String> answer = new ArrayList();
    public List<String> generateParenthesis(int n) {
        if (n <= 0){
            return answer;
        }
        generate("",n,n);
        return answer;
    }
    public void generate(String parenthesis,int left,int right){
        if (left == 0 && right == 0){
            answer.add(parenthesis);
            return;
        }
        if (left == right){
            generate(parenthesis + "(",left - 1,right);
        }
        else if (left < right){
            if (left > 0){
                generate(parenthesis + "(",left - 1,right);
            }
            generate(parenthesis + ")",left,right - 1);
        }

    }
}
//leetcode submit region end(Prohibit modification and deletion)

}