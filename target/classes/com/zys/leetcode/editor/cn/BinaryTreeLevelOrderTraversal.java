//给你一个二叉树，请你返回其按 层序遍历 得到的节点值。 （即逐层地，从左到右访问所有节点）。 
//
// 
//
// 示例： 
//二叉树：[3,9,20,null,null,15,7], 
//
// 
//    3
//   / \
//  9  20
//    /  \
//   15   7
// 
//
// 返回其层序遍历结果： 
//
// 
//[
//  [3],
//  [9,20],
//  [15,7]
//]
// 
// Related Topics 树 广度优先搜索 
// 👍 877 👎 0

package com.zys.leetcode.editor.cn;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinaryTreeLevelOrderTraversal {
    public static void main(String[] args) {
        Solution solution = new BinaryTreeLevelOrderTraversal().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
*/
class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        List<List<Integer>> answer = new ArrayList<List<Integer>>();
        int thisLevel = 1;
        int nextLevle = 0;
        List<Integer> levelNode = new ArrayList<>();
        TreeNode p;
        queue.offer(root);
        while ((p = queue.poll()) != null){
            levelNode.add(p.val);
            if (p.left != null){
                queue.offer(p.left);
                ++nextLevle;
            }
            if (p.right != null){
                queue.offer(p.right);
                ++nextLevle;
            }
            if(--thisLevel == 0){
                answer.add(new ArrayList<>(levelNode));
                levelNode = new ArrayList<>();
                thisLevel = nextLevle;
                nextLevle = 0;
            }
        }
        return answer;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}