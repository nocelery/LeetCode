//给定一个二叉树，找出其最大深度。 
//
// 二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。 
//
// 说明: 叶子节点是指没有子节点的节点。 
//
// 示例： 
//给定二叉树 [3,9,20,null,null,15,7]， 
//
//     3
//   / \
//  9  20
//    /  \
//   15   7 
//
// 返回它的最大深度 3 。 
// Related Topics 树 深度优先搜索 递归 
// 👍 862 👎 0

package com.zys.leetcode.editor.cn;
public class MaximumDepthOfBinaryTree {
    public static void main(String[] args) {
        Solution solution = new MaximumDepthOfBinaryTree().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.*/
//public class TreeNode {
//    int val;
//    TreeNode left;
//    TreeNode right;
//    TreeNode() {}
//    TreeNode(int val) { this.val = val; }
//    TreeNode(int val, TreeNode left, TreeNode right) {
//        this.val = val;
//        this.left = left;
//        this.right = right;
//    }
//}

class Solution {
    int maxDepth = 1;
    public int maxDepth(TreeNode root) {
        if (root == null){
            return 0;
        }
        calcuLevel(root.left,1);
        calcuLevel(root.right,1);
        return maxDepth;
    }
    public void calcuLevel(TreeNode t,int level){
        if (t != null){
            level++;
        }
        else {
            return;
        }
        maxDepth = Math.max(level,maxDepth);
        calcuLevel(t.left,level);
        calcuLevel(t.right,level);
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}