//给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。 
//
// 解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [1,2,3]
//输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
// 
//
// 示例 2： 
//
// 
//输入：nums = [0]
//输出：[[],[0]]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 10 
// -10 <= nums[i] <= 10 
// nums 中的所有元素 互不相同 
// 
// Related Topics 位运算 数组 回溯算法 
// 👍 1190 👎 0

package com.zys.leetcode.editor.cn;

import java.util.ArrayList;
import java.util.List;

public class Subsets {
    public static void main(String[] args) {
        Solution solution = new Subsets().new Solution();
        solution.subsets(new int[]{1,2,3});
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    List<List<Integer>> answer = new ArrayList<List<Integer>>();
    public List<List<Integer>> subsets(int[] nums) {
        sets(nums,0,new ArrayList<>());
        return answer;
    }
    public void sets(int[] nums,int in,List<Integer> set){
        if (in >= nums.length){
            answer.add(new ArrayList<>(set));
            return;
        }
//      当前元素加入子集，递归
        set.add(nums[in]);
        sets(nums,in + 1,set);
//      删除当前元素，再递归
        set.remove(set.size() - 1);
        sets(nums,in + 1,set);
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}