//请判断一个链表是否为回文链表。 
//
// 示例 1: 
//
// 输入: 1->2
//输出: false 
//
// 示例 2: 
//
// 输入: 1->2->2->1
//输出: true
// 
//
// 进阶： 
//你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？ 
// Related Topics 链表 双指针 
// 👍 939 👎 0

package com.zys.leetcode.editor.cn;

import java.util.List;

public class PalindromeLinkedList {
    public static void main(String[] args) {
        Solution solution = new PalindromeLinkedList().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)

//Definition for singly-linked list.
//class ListNode {
//    int val;
//    ListNode next;
//
//    ListNode() {
//    }
//
//    ListNode(int val) {
//        this.val = val;
//    }
//
//    ListNode(int val, ListNode next) {
//        this.val = val;
//        this.next = next;
//    }
//
//}
class Solution {
    public boolean isPalindrome(ListNode head) {
        ListNode temp;
        ListNode newHead = new ListNode();
        newHead.next = head;
        ListNode p1 = new ListNode();
        ListNode p2;
        int n = getListLength(newHead);
        if (n == 1){
            return true;
        }
        temp = newHead.next;
        int i = 0;
        while (temp != null){
            if (i >= n / 2){
                if (n % 2 == 1 && i == n / 2) {
                    temp = temp.next;
                }
                else {
                    p2 = temp.next;
                    temp.next = p1.next;
                    p1.next = temp;
                    temp = p2;
                }
            }
            else {
                temp = temp.next;
            }
            i++;
        }
        temp = newHead;
        while (p1.next != null){
            p1 = p1.next;
            temp = temp.next;
            if (p1.val != temp.val){
                return false;
            }
        }
        return true;
    }

    public int getListLength(ListNode head){
        int n = 0;
        while(head.next != null){
            head = head.next;
            n++;
        }
        return n;
    }
}

//leetcode submit region end(Prohibit modification and deletion)

}