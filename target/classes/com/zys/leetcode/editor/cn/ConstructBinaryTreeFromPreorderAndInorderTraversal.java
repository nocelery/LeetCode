//根据一棵树的前序遍历与中序遍历构造二叉树。 
//
// 注意: 
//你可以假设树中没有重复的元素。 
//
// 例如，给出 
//
// 前序遍历 preorder = [3,9,20,15,7]
//中序遍历 inorder = [9,3,15,20,7] 
//
// 返回如下的二叉树： 
//
//     3
//   / \
//  9  20
//    /  \
//   15   7 
// Related Topics 树 深度优先搜索 数组 
// 👍 1059 👎 0

package com.zys.leetcode.editor.cn;
public class ConstructBinaryTreeFromPreorderAndInorderTraversal {
    public static void main(String[] args) {
        Solution solution = new ConstructBinaryTreeFromPreorderAndInorderTraversal().new Solution();
        solution.buildTree(new int[]{3,9,20,15,7},new int[]{9,3,15,20,7});
    }
    //leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node. */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class Solution {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return build(preorder, inorder,0,0,inorder.length - 1);
    }
    public TreeNode build(int[] preorder, int[] inorder, int pi, int il, int ih){
        if (pi >= preorder.length){
            return null;
        }
        TreeNode root = new TreeNode(preorder[pi++]);
        if (il >= ih){
            return root;
        }
        int i = il;
        for (i = il; (preorder[pi] != inorder[i] && i < ih); ++i){}
        if (inorder[i] == preorder[pi]) {
            root.left = build(preorder, inorder, pi, il, i);
        }
        if (i < ih) {
            root.right = build(preorder, inorder, pi + i - il, i + 1, ih);
        }
        return root;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}