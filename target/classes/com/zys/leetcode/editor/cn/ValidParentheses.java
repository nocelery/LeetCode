//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。 
//
// 有效字符串需满足： 
//
// 
// 左括号必须用相同类型的右括号闭合。 
// 左括号必须以正确的顺序闭合。 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：s = "()"
//输出：true
// 
//
// 示例 2： 
//
// 
//输入：s = "()[]{}"
//输出：true
// 
//
// 示例 3： 
//
// 
//输入：s = "(]"
//输出：false
// 
//
// 示例 4： 
//
// 
//输入：s = "([)]"
//输出：false
// 
//
// 示例 5： 
//
// 
//输入：s = "{[]}"
//输出：true 
//
// 
//
// 提示： 
//
// 
// 1 <= s.length <= 104 
// s 仅由括号 '()[]{}' 组成 
// 
// Related Topics 栈 字符串 
// 👍 2328 👎 0

package com.zys.leetcode.editor.cn;

import java.util.*;

public class ValidParentheses {
    public static void main(String[] args) {
        Solution solution = new ValidParentheses().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
//        第一次自己想出来的方法
//    public boolean isValid(String s) {
//        char[] c = s.toCharArray();
//        Stack<Character> stack = new Stack<Character>();
//        if(!isLeftBracket(c[0]) || c.length < 2){
//            return false;
//        }
//        else{
//            stack.push(c[0]);
//        }
//        char temp;
//        int i = 1;
//        while(i < c.length){
//            if (isLeftBracket(c[i])){
//                stack.push(c[i]);
//            }
//            else {
//                if(stack.isEmpty()){
//                    return false;
//                }
//                temp = stack.pop();
//                if (!isCouple(temp,c[i])){
//                    return false;
//                }
//            }
//            i++;
//        }
//        if (i > c.length || !stack.isEmpty()){
//            return false;
//        }
//        else {
//            return true;
//        }
//    }
//
//    public boolean isLeftBracket(char c){
//        if (c == '(' || c == '{' || c == '['){
//            return true;
//        }
//        else {
//            return false;
//        }
//    }
//
//    public boolean isCouple(char c1, char c2){
//        if(c1 == '{' && c2 == '}'){
//            return true;
//        }
//        if(c1 == '(' && c2 == ')'){
//            return true;
//        }
//        if (c1 == '[' && c2 == ']'){
//            return true;
//        }
//        return false;
//    }
//
//    }

//        看来别人的解题思路之后的做法
    public boolean isValid(String s){
        int n = s.length();
        if (n % 2 == 1){
            return false;
        }
        Map<Character,Character> bracket = new HashMap<Character,Character>(){{
            put(')','(');put('}','{');put(']','[');
        }};
        Deque<Character> stack = new LinkedList<Character>();
        for (int i = 0; i < n; i++){
            char c = s.charAt(i);
            if (bracket.containsKey(c)){
                if (stack.isEmpty() || !stack.peek().equals(bracket.get(c))){
                    return false;
                }
                stack.pop();
            }
            else {
                stack.push(c);
            }
        }
        return stack.isEmpty();
    }
    }








//leetcode submit region end(Prohibit modification and deletion)

}