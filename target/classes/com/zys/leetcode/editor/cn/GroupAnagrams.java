//给定一个字符串数组，将字母异位词组合在一起。字母异位词指字母相同，但排列不同的字符串。 
//
// 示例: 
//
// 输入: ["eat", "tea", "tan", "ate", "nat", "bat"]
//输出:
//[
//  ["ate","eat","tea"],
//  ["nat","tan"],
//  ["bat"]
//] 
//
// 说明： 
//
// 
// 所有输入均为小写字母。 
// 不考虑答案输出的顺序。 
// 
// Related Topics 哈希表 字符串 
// 👍 735 👎 0

package com.zys.leetcode.editor.cn;

import java.util.*;

public class GroupAnagrams {
    public static void main(String[] args) {
        Solution solution = new GroupAnagrams().new Solution();
        solution.groupAnagrams(new String[]{"eat","tea","tan","ate","nat","bat"});
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String,List<String>> hash = new HashMap<>();
        List<List<String>> answer = new ArrayList<List<String>>();
        for (int i = 0; i < strs.length; ++i){
            char[] chars = strs[i].toCharArray();
            Arrays.sort(chars);
            String s = chars.toString();
            List<String> list;
            if (!hash.containsKey(s)){
                list = new ArrayList<>();
            }
            else {
                list = hash.get(chars);
            }
            list.add(strs[i]);
            hash.put(s,list);
        }
        hash.forEach((key,value) ->{
            answer.add(value);
        });
        return answer;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}